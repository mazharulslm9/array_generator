<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Array</title>
        <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-4 col-md-4">
                        <hr>
                        <form class="form-inline text-center" action="" method="post">
                            <div class="form-group">
                                <label for="exampleInputName2">Enter String::</label>
                                <input id="str" value="<?php if (isset($_POST['string'])) echo $_POST['string'] ?>" type="text" name="string" class="form-control" id="exampleInputName2" placeholder="Enter a String">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="submit" value="Click" name="submit">

                            </div>

                        </form>
                        <div id="message" class="text-center">
                            <label>Output:</label>
                            <h4>
                                <?php
                                if (isset($_POST['submit'])) {
                                    $string = $_POST['string'];
                                    $pieces = explode(" ", $string);
                                    foreach ($pieces as $key => $value) {
                                        echo $value;
                                        echo '<br>';
                                    }
                                }
                                ?>
                            </h4>
                        </div>
                    </div>

                </div>

            </div>
        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
            setTimeout(function () {
                $('#message').fadeOut('slow');
                $('#str').val('');

            }, 8000);

        </script>
    </body>
</html>
